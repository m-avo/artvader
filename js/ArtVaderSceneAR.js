import React, { Component } from 'react';
import { ViroARScene, ViroConstants } from 'react-viro';

import ArtItem from './component/ArtItem';
import Art3DObject from './component/Art3DObject';
import { IMAGE_ITEM_PARAMS } from './consts';

class ArtVaderSceneAR extends Component {
    state = {
        sound: {},
        showInfo: false,
    };

    _onInitialized = (state, reason) => {
        if (state == ViroConstants.TRACKING_NONE) {
            alert('Sorry.Something wend wrong!!!')
        }
    };

    _onSoundPress = (id) => {
        this.setState(prevState => ({
            sound: { [id]: !prevState.sound[id] }
        }))
    };

    render() {
        const { sound } = this.state;

        return (
            <ViroARScene onTrackingUpdated={this._onInitialized} >
                {
                    IMAGE_ITEM_PARAMS.map(item => (
                        <ArtItem
                            key={item.id}
                            isSound={sound[item.id]}
                            onSoundPress={() => this._onSoundPress(item.id)}
                            { ...item }
                        />
                    ))
                }
                <Art3DObject />
            </ViroARScene>
        );
    }
}

export default ArtVaderSceneAR;
