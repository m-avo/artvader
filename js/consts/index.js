const boxLength = 2.5;
const boxWidth = 1.2;
const Y = 0.3;

export const IMAGE_ITEM_PARAMS = [{
    id: 1,
    position: [0, Y, -boxLength],
    soundSource: require('../assets/saryan.mp3'),
    imageSource: require('../assets/saryan.jpg'),
    description: 'Martiros Saryan - Armenia',
    genre: 'Импрессионизм'
}, {
    id: 2,
    position: [boxLength, Y, -boxWidth],
    rotation: [0, -90, 0],
    soundSource: require('../assets/daft-punk.mp3'),
    imageSource: require('../assets/daft-punk.png'),
    description: 'Daft Punk - Veridis Quo',
    genre: 'Модернизм'
}, {
    id: 3,
    position: [boxLength, Y, boxWidth],
    rotation: [0, -90, 0],
    soundSource: require('../assets/xp.mp3'),
    imageSource: require('../assets/xp.jpg'),
    description: 'Windows XP Logoff',
    genre: 'Модернизм'
}, {
    id: 4,
    position: [0, Y, boxLength],
    rotation: [0, 180, 0],
    soundSource: require('../assets/magritte.mp3'),
    imageSource: require('../assets/magritte.jpg'),
    description: 'Rene Magritte - The Son of Man',
    genre: 'Экспрессионизм'
}, {
    id: 5,
    position: [-boxLength, Y, boxWidth],
    rotation: [0, 90, 0],
    soundSource: require('../assets/aivazovsky.mp3'),
    imageSource: require('../assets/aivazovsky.jpg'),
    description: 'Hovhannes Aivazovsky - 9th Wave',
    genre: 'Марина'

}, {
    id: 6,
    position: [-boxLength, Y, -boxWidth],
    rotation: [0, 90, 0],
    soundSource: require('../assets/shnur.mp3'),
    imageSource: require('../assets/shnur.jpg'),
    description: 'Leningrad - Dva Raza Net',
    genre: 'Абстракционизм'
}];