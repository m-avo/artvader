import React from 'react';
import PropTypes from 'prop-types';
import { ViroFlexView, ViroText } from 'react-viro';

 const InfoText = ({ text }) => (
    <ViroFlexView>
        <ViroText
            text={text}
            scale={[.6, .6, .6]}
            width={2}
            height={.2}
            style={{
                fontFamily: 'Arial',
                fontSize: 10,
                color: '#ffffff',
                textAlignVertical: 'center',
                textAlign: 'center',
            }}
        />
    </ViroFlexView>
);

 InfoText.propTypes = {
     text: PropTypes.string,
 };

export default InfoText;