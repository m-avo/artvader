import React, { Component } from 'react';
import { ViroButton, ViroFlexView } from 'react-viro';
import PropTypes from 'prop-types';
import like from '../../images/like.png';
import liked from '../../images/liked.png';
import dislike from '../../images/dislike.png';
import disliked from '../../images/disliked.png';
import styles from "./styles";

class RespectButton extends Component {
    render() {
        const { isLiked, isDisliked, onLikePress, onDislikePress } = this.props;

        return (
            <ViroFlexView style={styles.container}>
                <ViroFlexView width={0.4} height={0.22} style={{ flex: 1 }}>
                    <ViroButton
                        source={isLiked ? liked : like}
                        width={0.4}
                        height={0.22}
                        onClick={onLikePress}
                    />
                </ViroFlexView>
                <ViroFlexView width={0.4} height={0.22} style={{ flex: 1 }}>
                    <ViroButton
                        source={isDisliked ? disliked : dislike}
                        width={0.4}
                        height={0.22}
                        onClick={onDislikePress}
                    />
                </ViroFlexView>
            </ViroFlexView>
        );
    }
}

RespectButton.propTypes = {
    isLiked: PropTypes.bool,
    isDisliked: PropTypes.bool,
    likeSource: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    dislikeSource: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    onLikePress: PropTypes.func,
    onDislikePress: PropTypes.func,
};

RespectButton.defaultProps = {
    isLiked: false,
    isDisliked: false,
    likeSource: like,
    dislikeSource: dislike,
    onLikePress: () => {},
    onDislikePress: () => {},
};

export default RespectButton;
