import React, { Component } from 'react';
import { ViroImage, ViroFlexView, ViroText } from 'react-viro';
import PropTypes from 'prop-types';
import styles from './styles';
import demoImage from '../../images/demo.jpg';

import RespectButtons from '../RespectButtons';
import InfoPanel from '../InfoPanel';
import InfoText from '../InfoText';

class ArtItem extends Component {
    state = {
        showActionButtons: false,
        isLiked: false,
        isDisliked: false,
        showInfo: false,
    };

    _onClick = () => {
        this.setState(prevState => ({
            showActionButtons: !prevState.showActionButtons,
        }))
    };

    respectAction = async (value, label) => {
        const fd = new FormData();
        fd.append('value', value);
        fd.append('label', label);

        const response = await fetch(
            'http://172.20.38.202:5000/opinion',
            { method: 'POST', body: fd },
        );
        return response.status === 200;
    };

    _onLikePress = () => {
        const { genre } = this.props;
        this.setState(
            prevState => ({
                isLiked: prevState.isLiked || !prevState.isLiked,
                isDisliked: (prevState.isDisliked && !prevState.isLiked) ? !prevState.isDisliked : prevState.isDisliked,
            }), () => {
                this.respectAction(true, genre);
            }
        );
    };

    _onDislikePress = () => {
        const { genre } = this.props;
        this.setState(
            prevState => ({
                isDisliked: prevState.isDisliked || !prevState.isDisliked,
                isLiked: (!prevState.isDisliked && prevState.isLiked) ? !prevState.isLiked : prevState.isLiked,
            }), () => {
                this.respectAction(false, genre);
            }
        );
    };

    _onInfoPress = () => {
        this.setState(prevState => ({
            showInfo: !prevState.showInfo,
        }))
    };

    render(){
        const {
            position,
            rotation,
            imageDimension,
            viewDimension,
            isSound,
            onSoundPress,
            imageSource,
            soundSource,
            description,
        } = this.props;
        const { showInfo, showActionButtons, isDisliked, isLiked } = this.state;

        return (
            <ViroFlexView
                style={{ justifyContent: 'center', alignItems: 'center' }}
                position={position}
                rotation={rotation}
                { ...viewDimension }
                onClick={this._onClick}
            >
                { showInfo ? <InfoText text={description} /> : null }
                <ViroFlexView style={styles.container}>
                    { showActionButtons ? (
                        <InfoPanel
                            onInfoPress={this._onInfoPress}
                            onSoundPress={onSoundPress}
                            isSound={isSound}
                            soundSource={soundSource}
                        />
                    ) : null }
                    <ViroFlexView>
                        <ViroImage
                            source={imageSource}
                            { ...imageDimension }
                        />
                    </ViroFlexView>
                    { showActionButtons ? (
                        <RespectButtons
                            isLiked={isLiked}
                            isDisliked={isDisliked}
                            onLikePress={this._onLikePress}
                            onDislikePress={this._onDislikePress}
                        />
                    ) : null }
                </ViroFlexView>
            </ViroFlexView>
        );
    }
}

ArtItem.propTypes = {
    isSound: PropTypes.bool,
    position: PropTypes.array,
    rotation: PropTypes.array,
    imageDimension: PropTypes.shape({ width: PropTypes.number, height: PropTypes.number }),
    viewDimension: PropTypes.shape({ width: PropTypes.number, height: PropTypes.number }),
    imageSource: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    soundSource: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    onSoundPress: PropTypes.func,
    onInfoPress: PropTypes.func,
    description: PropTypes.string,
};

ArtItem.defaultProps = {
    isSound: false,
    position: [0, 0, 0],
    rotation: [0, 0, 0],
    imageDimension: { width: 0.8, height: 0.8 },
    viewDimension: { width: 2, height: 2 },
    imageSource: demoImage,
    soundSource: demoImage,
};

export default ArtItem;
