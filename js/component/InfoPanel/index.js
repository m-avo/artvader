import React, { Component } from 'react';
import { ViroButton, ViroFlexView, ViroSound } from 'react-viro';
import PropTypes from 'prop-types';
import mute from '../../images/mute.png';
import sound from '../../images/sound.png';
import info from '../../images/info.png';
import styles from "./styles";

class RespectButton extends Component {
    render() {
        const { isSound, onSoundPress, onInfoPress, soundSource } = this.props;

        return (
            <ViroFlexView style={styles.container}>
                <ViroSound
                    source={soundSource}
                    paused={!isSound}
                    loop
                />
                <ViroFlexView width={0.4} height={0.22} style={{ flex: 1 }}>
                    <ViroButton
                        source={info}
                        width={0.4}
                        height={0.22}
                        onClick={onInfoPress}
                    />
                </ViroFlexView>
                <ViroFlexView  width={0.4} height={0.22} style={{ flex: 1 }}>
                    <ViroButton
                        source={isSound ? sound : mute}
                        width={0.4}
                        height={0.22}
                        onClick={onSoundPress}
                    />
                </ViroFlexView>
            </ViroFlexView>
        );
    }
}

RespectButton.propTypes = {
    isSound: PropTypes.bool,
    soundSource: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    infoSource: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    onSoundPress: PropTypes.func,
    onInfoPress: PropTypes.func,
};

RespectButton.defaultProps = {
    isSound: false,
    muteSource: mute,
    sound: sound,
    onSoundPress: () => {},
    onInfoPress: () => {},
};

export default RespectButton;
