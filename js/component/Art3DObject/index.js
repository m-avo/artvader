import React from 'react';
import { Viro3DObject } from 'react-viro';

import objectSource from '../../3DObject/1669-02-raw.obj';
import resourcesMaterial from '../../3DObject/1669-02-raw.mtl';
import resourcesImage from '../../3DObject/1669-02-raw.jpg';

const Art3DObject = () => (
    <Viro3DObject
        source={objectSource}
        resources={[resourcesMaterial, resourcesImage]}
        position={[-3, -1, 3]}
        scale={[0.01, 0.01, 0.01]}
        rotation={[0, 65, 0]}
        type="OBJ"
    />
);

export default Art3DObject;
