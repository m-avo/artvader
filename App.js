/**
 * Copyright (c) 2017-present, Viro, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableHighlight,
} from 'react-native';
import map from 'lodash/map';
import get from 'lodash/get';
import {
    ViroARSceneNavigator
} from 'react-viro';
import InitialARScene from './js/ArtVaderSceneAR';

const sharedProps = { apiKey:"08FE77DB-5F23-4EF1-8D6F-D0702BF7D3EF" };
const UNSET = "UNSET";
const AR_NAVIGATOR_TYPE = "AR";
const defaultNavigatorType = UNSET;

export default class ViroSample extends Component {
    constructor() {
        super();

        this.state = {
            navigatorType: defaultNavigatorType,
            sharedProps: sharedProps,
            museumsData: [],
        };

        this._getExperienceSelector = this._getExperienceSelector.bind(this);
        this._getARNavigator = this._getARNavigator.bind(this);
        this._getExperienceButtonOnPress = this._getExperienceButtonOnPress.bind(this);
        this._exitViro = this._exitViro.bind(this);
    }

    _getARNavigator() {
        return (
            <ViroARSceneNavigator {...this.state.sharedProps}
                                  initialScene={{scene: InitialARScene}} />
        );
    }

    _getExperienceButtonOnPress(navigatorType) {
        return () => {
            this.setState({
                navigatorType : navigatorType
            })
        }
    }

    _exitViro() {
        this.setState({ navigatorType : UNSET })
    }

    getMuseumsData = async () => {
        const response = await fetch('http://172.20.38.202:5000/museums');
        const data = await response.json();
        this.setState({ museumsData: get(data, '[0].museums', []) })
    };

    componentDidMount() {
        this.getMuseumsData();
    }

    render() {
        if (this.state.navigatorType == UNSET) {
            return this._getExperienceSelector();
        } else if (this.state.navigatorType == AR_NAVIGATOR_TYPE) {
            return this._getARNavigator();
        }
    }

    _getExperienceSelector() {
        const { museumsData } = this.state;
        return (
            <View style={localStyles.outer} >
                <View style={localStyles.inner} >

                    <Text style={localStyles.titleText}>
                        Выберите музей:
                    </Text>
                    {
                        map(museumsData, (item, index) => (
                            <TouchableHighlight
                                key={index}
                                style={localStyles.buttons}
                                onPress={this._getExperienceButtonOnPress(AR_NAVIGATOR_TYPE)}
                                underlayColor={'#68a0ff'} >

                                <Text style={localStyles.buttonText}>{item}</Text>
                            </TouchableHighlight>
                        ))
                    }

                </View>
            </View>
        );
    }
}

const localStyles = StyleSheet.create({
    viroContainer :{
        flex : 1,
        backgroundColor: "black",
    },
    outer : {
        flex : 1,
        flexDirection: 'row',
        alignItems:'center',
        backgroundColor: "black",
    },
    inner: {
        flex : 1,
        flexDirection: 'column',
        alignItems:'center',
        backgroundColor: "black",
    },
    titleText: {
        paddingTop: 30,
        paddingBottom: 20,
        color:'#fff',
        textAlign:'center',
        fontSize : 25
    },
    buttonText: {
        color:'#fff',
        textAlign:'center',
        fontSize : 20
    },
    buttons : {
        height: 50,
        width: 220,
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
        backgroundColor:'#68a0cf',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
    },
    exitButton : {
        height: 50,
        width: 100,
        paddingTop:10,
        paddingBottom:10,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor:'#68a0cf',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
    }
});

module.exports = ViroSample;
